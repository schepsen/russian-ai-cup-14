import java.awt.*;
import java.awt.geom.Ellipse2D;

import model.*;

public final class LocalTestRendererListener
{
    public static final double MIN_ANGLE = 35.0, MAX_ANGLE = 55.0;
    protected static final double OFFENSIVE_Y1 = 250.0D;
    protected static final double OFFENSIVE_Y2 = 670.0D;

    public void beforeDrawScene(Graphics graphics, World world, Game game, double scale)
    {
        double angle;
        boolean isOptLocation;
        for (int XS = 65; XS < 600; XS += 5)
        {
            for (int YS = 150; YS < 560; YS += 5)
            {
                graphics.setColor(Color.BLUE);
                angle = Math.atan(Math.abs(XS - 65.0) / Math.abs(YS - 560.0));
                isOptLocation = angle >= Math.toRadians(MIN_ANGLE) && angle <= Math.toRadians(MAX_ANGLE);
                if (isOptLocation)
                {
                    graphics.drawLine((int) XS, (int) YS, (int) XS, (int) YS);
                }
            }
        }

        for (int XS = 65; XS < 600; XS += 5)
        {
            for (int YS = 360; YS < 770; YS += 5)
            {
                graphics.setColor(Color.RED);
                angle = Math.atan(Math.abs(XS - 65.0) / Math.abs(YS - 360.0));
                isOptLocation = angle >= Math.toRadians(MIN_ANGLE) && angle <= Math.toRadians(MAX_ANGLE);
                if (isOptLocation)
                {
                    graphics.drawLine((int) XS, (int) YS, (int) XS, (int) YS);
                }
            }
        }

        graphics.setColor(Color.BLACK);
        
//        Ellipse2D a = new Ellipse2D.Double(550, 170, 240.0, 240.0);
//        Ellipse2D b = new Ellipse2D.Double(550, 510, 240.0, 240.0);
//        Ellipse2D c = new Ellipse2D.Double(410, 170, 240.0, 240.0);
//        Ellipse2D d = new Ellipse2D.Double(410, 510, 240.0, 240.0);
//        
//        Graphics2D g2 = (Graphics2D) graphics;
//        g2.draw(a);
//        g2.draw(b);
//        g2.draw(c);
//        g2.draw(d);
    }

    public void afterDrawScene(Graphics graphics, World world, Game game, double scale)
    {

        System.out.printf("%.3f %.3f %.3f %.3f\n", world.getPuck().getX(), world.getPuck().getY(), world.getPuck().getSpeedX(), world.getPuck().getSpeedY());

        Font ubuntu = new Font("Ubuntu", Font.PLAIN, 14);
        graphics.setFont(ubuntu);

        graphics.drawLine(600, 150, 600, 770);
        graphics.drawLine(65, 460, 1135, 460);

        graphics.drawOval(270 - 2, (int) OFFENSIVE_Y1 - 2, 4, 4);
        graphics.drawOval(270 - 2, (int) OFFENSIVE_Y2 - 2, 4, 4);
        graphics.drawOval(920 - 2, (int) OFFENSIVE_Y1 - 2, 4, 4);
        graphics.drawOval(920 - 2, (int) OFFENSIVE_Y2 - 2, 4, 4);

        double length = 300.0, rad;
        int aX, aY, bX, bY, cX, cY;

        double speed, speedX = world.getPuck().getSpeedX(), speedY = world.getPuck().getSpeedY(), X, Y;
        X = world.getPuck().getX();
        Y = world.getPuck().getY();
        speed = Math.hypot(world.getPuck().getSpeedX(), world.getPuck().getSpeedY());

        graphics.drawString(String.format("Speed: %.2f (X = %.2f; Y = %.2f) @ [X = %.2f; Y = %.2f]", speed, speedX, speedY, X, Y), 200, 100);

        
        
        for (Hockeyist h : world.getHockeyists())
        {

            if (world.getPuck().getOwnerHockeyistId() == h.getId())
            {
                graphics.drawLine((int) h.getX(), (int) h.getY(), (int) (h.getX() + Math.cos(h.getAngle()) * 120), (int) (h.getY() + Math.sin(h.getAngle()) * 120));
            }
            if (h.getType() != HockeyistType.GOALIE)
            {
                graphics.setColor(Color.GRAY);
                graphics.drawOval((int) h.getX() - 120, (int) h.getY() - 120, 240, 240);
                graphics.setColor(Color.BLACK);
            }
            graphics.drawString(String.format("X = %.2f; Y = %.2f; T = %d\n", h.getX(), h.getY(), h.getRemainingCooldownTicks()), (int) h.getX() - 50, (int) h.getY() - 50);
        }
    }
}
