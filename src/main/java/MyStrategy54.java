import java.awt.Polygon;

import model.*;

class Config
{
    protected static final int STRIKE_WAITING_TICKS = 10;

    protected static final double HALFWAY_X = 600.0D;
    protected static final double HALFWAY_Y = 460.0D;
    protected static final double EVASION = 70.0D;
    protected static final double OFFENSIVE_Y1 = 360.0D;
    protected static final double OFFENSIVE_Y2 = 560.0D;
    protected static final double CORRECTION = 10.0D;

    protected static final double OPT_STRIKE_ANGLE = 45.0D;
    public static final double MIN_ANGLE = 35.0, MAX_ANGLE = 70.0;
}

enum Location
{
    CLOSE_TO_GOAL, CLOSE_TO_PUCK, CLOSE_TO_NONE
}

public final class MyStrategy54 implements Strategy
{
    @Override
    public void move(Hockeyist self, World world, Game game, Move move)
    {
        int sign = (world.getMyPlayer().getNetBack() < Config.HALFWAY_X) ? 1 : -1;

        double X = self.getX(), Y = self.getY();

        Puck puck = world.getPuck();
        Location position = null;

        double defX = world.getMyPlayer().getNetBack() + sign * 195.0D, defY = Config.HALFWAY_Y;

        if (isClosestTo(self, defX, defY, world))
        {
            position = Location.CLOSE_TO_GOAL;
        }
        else if (isClosestToPuck(self, world))
        {
            position = Location.CLOSE_TO_PUCK;
        }
        else
        {
            position = Location.CLOSE_TO_NONE;
        }

        double puckAngle = getAngleTo(self, puck);

        double oppGoalX = world.getOpponentPlayer().getNetFront();
        double oppGoalY = Config.HALFWAY_Y + (((Y < Config.HALFWAY_Y) ? 1 : -1) * 100.0 - Config.CORRECTION);

        if (self.getState() == HockeyistState.SWINGING)
        {
            if (self.getSwingTicks() > Config.STRIKE_WAITING_TICKS || self.getDistanceTo(oppGoalX, oppGoalY) <= 200.0)
            {
                move.setAction(ActionType.STRIKE);
            }
            else if (world.getPuck().getOwnerHockeyistId() != self.getId())
            {
                move.setAction(ActionType.CANCEL_STRIKE);
            }
            return;
        }

        double goalDistance = self.getDistanceTo(oppGoalX, oppGoalY);
        double goalAngle = self.getAngleTo(oppGoalX, oppGoalY);

        double offY = (Y < Config.HALFWAY_Y) ? Config.OFFENSIVE_Y1 : Config.OFFENSIVE_Y2;
        double offX = Config.HALFWAY_X + sign * (535.0 - (Math.abs(offY - oppGoalY) * Math.tan(Math.toRadians(Config.OPT_STRIKE_ANGLE))));

        Polygon defZone = new Polygon();

        defZone.addPoint((int) (world.getMyPlayer().getNetFront()), 150);
        defZone.addPoint((int) Config.HALFWAY_X, 150);
        defZone.addPoint((int) Config.HALFWAY_X, 770);
        defZone.addPoint((int) (world.getMyPlayer().getNetFront()), 770);

        int fitPlayerId;
        if (self.getStamina() <= 400.0 && (fitPlayerId = getFitHockeyistIndex(world.getHockeyists())) != -1 && puck.getOwnerHockeyistId() != self.getId())
        {
            substitute(self, fitPlayerId, sign, defZone, move);
            return;
        }

        if ((world.getMyPlayer().isJustMissedGoal() || world.getMyPlayer().isJustScoredGoal()) && (self.getStamina() <= 500.0 && (fitPlayerId = getFitHockeyistIndex(world.getHockeyists())) != -1))
        {
            substitute(self, fitPlayerId, sign, defZone, move);
            return;
        }

        double angle = Math.atan(Math.abs(X - oppGoalX) / Math.abs(Y - oppGoalY));
        boolean isOptLocation = angle >= Math.toRadians(Config.MIN_ANGLE) && angle <= Math.toRadians(Config.MAX_ANGLE);

        switch (position)
        {
            case CLOSE_TO_PUCK :
                move.setTurn(puckAngle);
                move.setSpeedUp(1.0D);

                if (puck.getOwnerPlayerId() != self.getPlayerId())
                {
                    if (puck.getOwnerPlayerId() == -1)
                    {
                        if (goalDistance <= 435.0 && isOptLocation && Math.abs(goalAngle) <= Math.toRadians(3.0))
                        {
                            strike(self, puck, move, game);
                        }
                        else
                        {
                            move.setAction(ActionType.TAKE_PUCK);
                        }
                    }
                    else
                    {
                        strike(self, puck, move, game);
                    }
                }
                else
                {
                    if (self.getDistanceTo(oppGoalX, oppGoalY) <= 500.0)
                    {
                        if (isOptLocation)
                        {
                            if (Math.abs(self.getAngleTo(oppGoalX, oppGoalY)) <= Math.toRadians(1.0D))
                            {
                                move.setAction(ActionType.SWING);
                            }
                            else
                            {
                                move.setTurn(self.getAngleTo(oppGoalX, oppGoalY));
                            }
                        }
                        else
                        {
                            passToTeammate(self, world, move, game, oppGoalX, oppGoalY);
                        }
                    }
                    else
                    {
                        if ((Y > (offY - 60.0D)) && (Y < Config.HALFWAY_Y))
                        {
                            move.setTurn(self.getAngleTo(offX, offY) - sign * Math.toRadians(Config.EVASION));
                        }
                        else if ((Y < (offY + 60.0D)) && (Y > Config.HALFWAY_Y))
                        {
                            move.setTurn(self.getAngleTo(offX, offY) + sign * Math.toRadians(Config.EVASION));
                        }
                        else
                        {
                            move.setTurn(self.getAngleTo(offX, offY));
                        }
                    }
                }
                break;
            case CLOSE_TO_GOAL :
                if (puck.getOwnerPlayerId() != self.getPlayerId() && defZone.contains(puck.getX(), puck.getY()))
                {
                    move.setTurn(puckAngle);
                    move.setSpeedUp(1.0D);
                }
                else
                {
                    retreatTo(self, defX, defY, world, move);
                }
                break;
            case CLOSE_TO_NONE :
                if (puck.getOwnerPlayerId() != self.getPlayerId() && puck.getOwnerPlayerId() != -1)
                {
                    move.setTurn(puckAngle);
                    move.setSpeedUp(1.0D);
                    strike(self, puck, move, game);
                }
                else
                {
                    if (self.getDistanceTo(Config.HALFWAY_X - sign * 50, Config.HALFWAY_Y + ((puck.getY() < Config.HALFWAY_Y) ? 0 : 0)) < 60.0)
                    {
                        move.setTurn(goalAngle);
                        move.setSpeedUp(0.0D);
                    }
                    else
                    {
                        retreatTo(self, Config.HALFWAY_X - sign * 50, Config.HALFWAY_Y + ((puck.getY() < Config.HALFWAY_Y) ? 0 : 0), world, move);
                    }
                }
                break;
            default :
                break;
        }
    }
    private int getFitHockeyistIndex(Hockeyist[] hockeyists)
    {
        double stamina, id = -1, max = Double.MIN_VALUE;
        for (Hockeyist hockeyist : hockeyists)
        {
            if (hockeyist.getState() == HockeyistState.RESTING && (stamina = hockeyist.getStamina()) >= 750 && hockeyist.isTeammate())
            {
                if (stamina > max)
                {
                    max = stamina;
                    id = hockeyist.getTeammateIndex();
                }

            }
        }
        return (int) id;
    }

    private static double getAngleTo(Hockeyist self, Puck puck)
    {
        double puckX = puck.getX() + 5 * puck.getSpeedX();
        double puckY = puck.getY() + 5 * puck.getSpeedY();

        return self.getAngleTo(puckX, puckY);
    }

    private static void strike(Hockeyist self, Unit unit, Move move, Game game)
    {
        if (self.getDistanceTo(unit) <= game.getStickLength() && Math.abs(self.getAngleTo(unit)) <= 0.5 * game.getStickSector())
        {
            move.setAction(ActionType.STRIKE);
        }
    }

    private static void substitute(Hockeyist self, int teammate, int sign, Polygon home, Move move)
    {
        double X = (home.contains(self.getX(), self.getY())) ? self.getX() : Config.HALFWAY_X - sign * 300.0;
        if ((self.getY() - 150.0) < 80.0 && home.contains(X, self.getY()))
        {
            move.setSpeedUp(0.1);
            if ((self.getY() - 150.0) < 60.0)
            {
                move.setTeammateIndex(teammate);
                move.setAction(ActionType.SUBSTITUTE);
            }
        }
        else
        {
            move.setTurn(self.getAngleTo(X, 150.0));
            move.setSpeedUp(1.0);
        }
    }

    private static boolean isClosestToPuck(Hockeyist self, World world)
    {
        double distance, min = Double.MAX_VALUE;
        for (Hockeyist hockeyist : world.getHockeyists())
        {
            if (hockeyist.getType() != HockeyistType.GOALIE && hockeyist.isTeammate() && hockeyist.getState() != HockeyistState.RESTING)
            {
                if ((distance = hockeyist.getDistanceTo(world.getPuck())) < min)
                {
                    min = distance;
                }
            }
        }
        return (self.getDistanceTo(world.getPuck()) <= min) ? true : false;
    }

    private static boolean isClosestTo(Hockeyist self, double x, double y, World world)
    {
        if (isClosestToPuck(self, world))
        {
            return false;
        }

        double distance, min = Double.MAX_VALUE;
        for (Hockeyist hockeyist : world.getHockeyists())
        {
            if (hockeyist.getType() != HockeyistType.GOALIE && hockeyist.isTeammate() && !isClosestToPuck(hockeyist, world) && hockeyist.getState() != HockeyistState.RESTING)
            {
                if ((distance = hockeyist.getDistanceTo(x, y)) < min)
                {
                    min = distance;
                }
            }
        }

        return (self.getDistanceTo(x, y) <= min) ? true : false;
    }

    private static void retreatTo(Hockeyist self, double x, double y, World world, Move move)
    {
        if (self.getDistanceTo(x, y) < 60.0D)
        {
            move.setTurn(self.getAngleTo(world.getPuck()));
            move.setSpeedUp(0.0D);
            move.setAction(ActionType.TAKE_PUCK);
        }
        else
        {
            double angle = self.getAngleTo(x, y);
            if ((Math.abs(angle) > Math.toRadians(90.0D)) && (self.getDistanceTo(x, y) < 300.0D))
            {
                move.setSpeedUp(-1.0D);
                move.setTurn(angle - (((angle > 0.0D) ? 1 : -1) * Math.toRadians(180)));
            }
            else
            {
                move.setTurn(angle);
                move.setSpeedUp(1.0D);
            }
        }
    }

    private static void passToTeammate(Hockeyist self, World world, Move move, Game game, double altX, double altY)
    {
        double angle = 0.0D, distance;
        Hockeyist teammate = null;
        for (Hockeyist hockeyist : world.getHockeyists())
        {
            if (hockeyist.isTeammate() && self.getDistanceTo(hockeyist) > 120.0D && hockeyist.getType() != HockeyistType.GOALIE && hockeyist.getState() != HockeyistState.RESTING)
            {
                distance = self.getDistanceTo(hockeyist);
                boolean verdeckt = false;
                angle = self.getAngleTo(hockeyist);
                for (Hockeyist rival : world.getHockeyists())
                {
                    if (!rival.isTeammate() && rival.getType() != HockeyistType.GOALIE && self.getDistanceTo(rival) <= distance && hockeyist.getState() != HockeyistState.RESTING)
                    {
                        if (Math.toDegrees(Math.abs(self.getAngleTo(hockeyist) - self.getAngleTo(rival))) < 5.0D)
                        {
                            verdeckt = true;
                            break;
                        }
                    }
                }
                if (!verdeckt)
                {
                    teammate = hockeyist;
                    break;
                }

            }
        }
        if (teammate != null)
        {
            if (Math.abs(angle) <= (0.5D * game.getPassSector()))
            {
                move.setPassPower(0.3D + self.getDistanceTo(teammate) * 0.001D);
                move.setPassAngle(angle);
                move.setAction(ActionType.PASS);
            }
            else
            {
                move.setTurn(Math.abs(angle - (0.5D * game.getPassSector())) * ((angle > Math.toRadians(0) ? 1 : -1)));
            }
        }
        else
        {
            if (Math.abs(self.getAngleTo(altX, altY)) <= (0.5D * game.getPassSector()))
            {
                move.setPassPower(1.0D);
                move.setPassAngle(self.getAngleTo(altX, altY));
                move.setAction(ActionType.PASS);
            }
            else
            {
                if (self.getDistanceTo(world.getPuck()) <= game.getStickLength() && Math.abs(self.getAngleTo(world.getPuck())) <= 0.5D * game.getStickSector())
                {
                    move.setAction(ActionType.STRIKE);
                }
            }
        }
    }
}