from time import sleep

from os import system
from os.path import isfile
from sys import argv, exit

VERSION1 = "previous"
VERSION2 = "beta"

def main():
  print("| -------- | ---------- | ----------- | ----------- | ----------- | ----------- |")
  print("| + GAME + | + STATUS + | + PLACE 1 + | + PLACE 2 + | + SCORE 1 + | + SCORE 2 + |")
  print("| -------- | ---------- | ----------- | ----------- | ----------- | ----------- |")
  try:
    wins = 0
    ties = 0
    for i in range(int(argv[1])):
      if(len(argv) == 2):
	system("java -jar local-runner.jar local-runner.properties &")
	sleep(1)
	system("java -cp " + VERSION1 + "/main/java Runner &")
	sleep(1)      
	system("java -cp " + VERSION2 + "/main/java Runner")
      else:
        system("java -jar local-runner.jar local-runner-quick.properties &")
        sleep(1)
        if(int(argv[2]) == 1):      
	  system("java -cp " + VERSION1 + "/main/java Runner")
        else:  
	  system("java -cp " + VERSION2 + "/main/java Runner 0 31001 0000000000000000")      
      while not isfile("result.txt"):
        pass
      sleep(1)
      results = open("result.txt")      
      data = results.read().split("\n")     
      if(int(data[2].split()[0]) == 2):
	wins += 1
      elif(int(data[2].split()[0]) == int(data[3].split()[0])):
	ties += 1
      print("| + " + str(i).zfill(4) + " + | +++ " + data[0] + " +++ | ++++ " + data[2].split()[0] + " ++++ | ++++ " + data[3].split()[0] + " ++++ | ++++ " + data[2].split()[1] + " ++++ | ++++ " + data[3].split()[1] + " ++++ |")      
      sleep(1)
      system("rm -rf result.txt")
      sleep(1)
    print("| -------- | ---------- | ----------- | ----------- | ----------- | ----------- |")
    print("| RESULT: You won in " + str(wins) + " of " + argv[1] + " game(s) (" + str(wins * 100 / int(argv[1])) + " %) [Ties: "+ str(ties) +" (" + str(ties * 100 / int(argv[1])) + " %); Loses: " + str(int(argv[1]) - wins - ties) + " (" + str((int(argv[1]) - wins - ties) * 100 / int(argv[1])) + " %)]")    
  except:
    exit(0)
if __name__ == "__main__": 
    main()