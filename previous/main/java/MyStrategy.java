import model.*;

import java.awt.Polygon;
import java.util.ArrayList;

class Config
{
    protected static final int STRIKE_WAITING_TICKS = 10;

    protected static final double HALFWAY_X = 600.0;
    protected static final double HALFWAY_Y = 460.0;
    protected static final double OFFENSIVE_Y1 = 250.0;
    protected static final double OFFENSIVE_Y2 = 670.0;
    protected static final double CORRECTION = 90.0;
    protected static final double MIN_STAMINA = 700.0;

    public static final double MIN_ANGLE = 30.0, MAX_ANGLE = 60;
}

enum Location
{
    CLOSE_TO_GOAL, CLOSE_TO_PUCK, CLOSE_TO_NONE
}

public final class MyStrategy implements Strategy
{
    @Override
    public void move(Hockeyist self, World world, Game game, Move move)
    {
        int sign = (world.getMyPlayer().getNetBack() < Config.HALFWAY_X) ? 1 : -1;

        double X = self.getX(), Y = self.getY();
        Puck puck = world.getPuck();
        Location position = null;
        double puckAngle = getAngleTo(self, puck);

        double defX = world.getMyPlayer().getNetBack() + sign * 195.0D, defY = Config.HALFWAY_Y;

        if (isClosestTo(self, defX, defY, world))
        {
            position = Location.CLOSE_TO_GOAL;
        }
        else if (isClosestToPuck(self, world) || puck.getOwnerHockeyistId() == self.getId())
        {
            position = Location.CLOSE_TO_PUCK;
        }
        else
        {
            position = Location.CLOSE_TO_NONE;
        }

        double oppGoalX = world.getOpponentPlayer().getNetFront();
        double oppGoalY = Config.HALFWAY_Y + (((Y < Config.HALFWAY_Y) ? 1 : -1) * Config.CORRECTION);

        if (self.getState() == HockeyistState.SWINGING)
        {
            if (self.getSwingTicks() >= Config.STRIKE_WAITING_TICKS)
            {
                move.setAction(ActionType.STRIKE);
            }
            else if (world.getPuck().getOwnerHockeyistId() != self.getId())
            {
                move.setAction(ActionType.CANCEL_STRIKE);
            }
            return;
        }

        double goalAngle = self.getAngleTo(oppGoalX, oppGoalY);

        double offX = Config.HALFWAY_X + sign * 320.0;
        double offY = ((Y + Math.sin(self.getAngle()) * game.getStickLength()) < Config.HALFWAY_Y) ? Config.OFFENSIVE_Y1 : Config.OFFENSIVE_Y2;

        Polygon defZone = new Polygon();

        defZone.addPoint((int) (world.getMyPlayer().getNetFront()), 150);
        defZone.addPoint((int) Config.HALFWAY_X, 150);
        defZone.addPoint((int) Config.HALFWAY_X, 770);
        defZone.addPoint((int) (world.getMyPlayer().getNetFront()), 770);

        if ((world.getMyPlayer().isJustMissedGoal() || world.getMyPlayer().isJustScoredGoal()))
        {
            Hockeyist victim = getActiveHockeyists(world.getHockeyists(), false)[0];
            move.setTurn(self.getAngleTo(victim));
            move.setSpeedUp(1.0);
            strike(self, victim, move, game);
            return;
        }

        int tmId = getFitHockeyistId(world.getHockeyists());

        if (self.getStamina() < Config.MIN_STAMINA && tmId != -1 && puck.getOwnerHockeyistId() != self.getId())
        {
            substitute(self, tmId, sign, defZone, move);
            return;
        }

        double fX = X + (10 - 0.07) * self.getSpeedX(), fY = Y + (10 - 0.07) * self.getSpeedY();
        double angle = Math.atan(Math.abs(fX - oppGoalX) / Math.abs(fY - oppGoalY));

        switch (position)
        {
            case CLOSE_TO_PUCK :
                if (puck.getOwnerPlayerId() != self.getPlayerId())
                {
                    move.setTurn(puckAngle);
                    move.setSpeedUp(1.0D);

                    if (puck.getOwnerPlayerId() == -1)
                    {
                        if (angle > Math.toRadians(Config.MIN_ANGLE) && angle < Math.toRadians(Config.MAX_ANGLE) && Math.abs(goalAngle) < Math.toRadians(1.0D))
                        {
                            strike(self, puck, move, game);
                        }
                        else
                        {
                            move.setAction(ActionType.TAKE_PUCK);
                        }
                    }
                    else
                    {
                        strike(self, puck, move, game);
                    }
                }
                else
                {
                    if (self.getRemainingCooldownTicks() > 0 && Math.abs(X - oppGoalX) < 300)
                    {
                        move.setTurn(self.getAngleTo(Config.HALFWAY_X - sign * 200, offY));
                        move.setSpeedUp(1.0);
                    }
                    else if (angle > Math.toRadians(Config.MIN_ANGLE) && angle < Math.toRadians(Config.MAX_ANGLE))
                    {
                        if (Math.abs(goalAngle - self.getAngularSpeed() * 10) > Math.toRadians(1.0D))
                        {
                            move.setTurn(goalAngle);
                        }
                        else
                        {
                            move.setAction(ActionType.SWING);
                        }
                    }
                    else
                    {
                        move.setSpeedUp(1.0D);
                        Hockeyist nearest = getNearestHockeyist(self, false, getActiveHockeyists(world.getHockeyists(), false));

                        double aheadX = 0.0, aheadY = 0.0;
                        double degree, increment = 0, degSign = (self.getAngleTo(nearest) <= 0) ? 1 : -1;

                        boolean verdeckt = true;

                        move.setTurn(self.getAngleTo(offX, offY));
                        while (verdeckt)
                        {
                            degree = self.getAngle() + Math.toRadians(degSign * increment);

                            aheadX = X + Math.cos(degree) * game.getStickLength();
                            aheadY = Y + Math.sin(degree) * game.getStickLength();

                            verdeckt = (nearest.getDistanceTo(aheadX, aheadY) <= 120) ? true : false;

                            if (increment > 180)
                                break;
                            if (increment > 0)
                                move.setTurn(self.getAngleTo(aheadX, aheadY));

                            increment += 1;
                        }
                    }
                }
                break;
            case CLOSE_TO_GOAL :
                if (puck.getOwnerPlayerId() != self.getPlayerId() && defZone.contains(puck.getX(), puck.getY()))
                {
                    move.setTurn(puckAngle);
                    move.setSpeedUp(0.5D);
                }
                else
                {
                    retreatTo(self, defX, defY, world, move);
                }
                break;
            case CLOSE_TO_NONE :
                if (puck.getOwnerPlayerId() != self.getPlayerId() && puck.getOwnerPlayerId() != -1)
                {
                    move.setTurn(puckAngle);
                    move.setSpeedUp(1.0D);
                    strike(self, puck, move, game);
                }
                else
                {
                    double N_X = Config.HALFWAY_X - sign * 100;
                    double N_Y = Config.HALFWAY_Y + ((puck.getY() < Config.HALFWAY_Y) ? 1 : -1) * 0;

                    if (self.getDistanceTo(N_X, N_Y) < 60.0)
                    {
                        move.setTurn(goalAngle);
                        move.setSpeedUp(0.0D);
                    }
                    else
                    {
                        retreatTo(self, N_X, N_Y, world, move);
                    }

                }
                break;
            default :
                break;
        }
    }

    private static Hockeyist getNearestHockeyist(Hockeyist self, boolean isTeammate, Hockeyist[] hockeyists)
    {
        double distance, min = Double.MAX_VALUE;
        Hockeyist nearest = null;
        for (Hockeyist hockeyist : hockeyists)
        {
            if (hockeyist.isTeammate() == isTeammate && (distance = self.getDistanceTo(hockeyist)) < min)
            {
                min = distance;
                nearest = hockeyist;
            }
        }
        return nearest;
    }

    private static Hockeyist[] getActiveHockeyists(Hockeyist[] hockeyists, boolean isTeammate)
    {
        ArrayList<Hockeyist> team = new ArrayList<>();
        for (Hockeyist hockeyist : hockeyists)
        {
            if (hockeyist.getState() != HockeyistState.RESTING && hockeyist.getType() != HockeyistType.GOALIE)
            {
                if (hockeyist.isTeammate() == isTeammate)
                {
                    team.add(hockeyist);
                }
            }
        }
        return team.toArray(new Hockeyist[team.size()]);
    }

    private int getFitHockeyistId(Hockeyist[] hockeyists)
    {
        double stamina, id = -1, max = Double.MIN_VALUE;
        for (Hockeyist player : hockeyists)
        {
            if (player.getState() == HockeyistState.RESTING && (stamina = player.getStamina()) >= Config.MIN_STAMINA * 1.25 && player.isTeammate())
            {
                if (stamina > max)
                {
                    max = stamina;
                    id = player.getTeammateIndex();
                }
            }
        }
        return (int) id;
    }

    private static double getAngleTo(Hockeyist self, Puck puck)
    {
        double puckX = puck.getX() + 5 * puck.getSpeedX();
        double puckY = puck.getY() + 5 * puck.getSpeedY();

        return self.getAngleTo(puckX, puckY);
    }

    private static void strike(Hockeyist self, Unit unit, Move move, Game game)
    {
        if (self.getDistanceTo(unit) <= game.getStickLength() && Math.abs(self.getAngleTo(unit)) <= 0.5 * game.getStickSector())
        {
            move.setAction(ActionType.STRIKE);
        }
    }

    private static void substitute(Hockeyist self, int teammate, int sign, Polygon home, Move move)
    {
        double X = (home.contains(self.getX(), self.getY())) ? self.getX() : Config.HALFWAY_X - sign * 300.0;
        if (Math.abs((self.getY() - 150.0)) < 80.0 && home.contains(self.getX(), self.getY()))
        {
            move.setSpeedUp(0.5);
            if (Math.abs(self.getY() - 150.0) < 60.0)
            {
                move.setTeammateIndex(teammate);
                move.setAction(ActionType.SUBSTITUTE);
            }
        }
        else
        {
            move.setTurn(self.getAngleTo(X, 150.0));
            move.setSpeedUp(1.0);
        }
    }

    private static boolean isClosestToPuck(Hockeyist self, World world)
    {
        double distance, min = Double.MAX_VALUE;
        for (Hockeyist hockeyist : world.getHockeyists())
        {
            if (hockeyist.getType() != HockeyistType.GOALIE && hockeyist.isTeammate() && hockeyist.getState() != HockeyistState.RESTING)
            {
                if ((distance = hockeyist.getDistanceTo(world.getPuck())) < min)
                {
                    min = distance;
                }
            }
        }
        return (self.getDistanceTo(world.getPuck()) <= min) ? true : false;
    }

    private static boolean isClosestTo(Hockeyist self, double x, double y, World world)
    {
        if (isClosestToPuck(self, world))
        {
            return false;
        }

        double distance, min = Double.MAX_VALUE;
        for (Hockeyist hockeyist : world.getHockeyists())
        {
            if (hockeyist.getType() != HockeyistType.GOALIE && hockeyist.isTeammate() && !isClosestToPuck(hockeyist, world) && hockeyist.getState() != HockeyistState.RESTING)
            {
                if ((distance = hockeyist.getDistanceTo(x, y)) < min)
                {
                    min = distance;
                }
            }
        }

        return (self.getDistanceTo(x, y) <= min) ? true : false;
    }

    private static void retreatTo(Hockeyist self, double x, double y, World world, Move move)
    {
        if (self.getDistanceTo(x, y) < 60.0D)
        {
            move.setTurn(self.getAngleTo(world.getPuck()));
            move.setSpeedUp(0.0D);
            move.setAction(ActionType.TAKE_PUCK);
        }
        else
        {
            double angle = self.getAngleTo(x, y);
            if ((Math.abs(angle) > Math.toRadians(90.0D)) && (self.getDistanceTo(x, y) < 300.0D))
            {
                move.setSpeedUp(-1.0D);
                move.setTurn(angle - (((angle > 0.0D) ? 1 : -1) * Math.toRadians(180)));
            }
            else
            {
                move.setTurn(angle);
                move.setSpeedUp(1.0D);

                Hockeyist nearest = getNearestHockeyist(self, true, getActiveHockeyists(world.getHockeyists(), true));

                double aheadX = 0.0, aheadY = 0.0;
                double degree, increment = 0, degSign = (self.getAngleTo(nearest) <= 0) ? 1 : -1;

                boolean verdeckt = true;

                while (verdeckt)
                {
                    degree = self.getAngle() + Math.toRadians(degSign * increment);

                    aheadX = self.getX() + Math.cos(degree) * 120;
                    aheadY = self.getY() + Math.sin(degree) * 120;

                    verdeckt = (nearest.getDistanceTo(aheadX, aheadY) <= 120) ? true : false;

                    if (increment > 180)
                        break;
                    if (increment > 0)
                        move.setTurn(self.getAngleTo(aheadX, aheadY));

                    increment += 1;
                }
            }
        }
    }

    private static void pass(Hockeyist self, Hockeyist teammate, Move move, Game game)
    {
        if (Math.abs(self.getAngleTo(teammate)) <= 0.5 * game.getPassSector())
        {
            move.setPassAngle(self.getAngleTo(teammate));
            move.setAction(ActionType.PASS);
            move.setPassPower(0.4 + (self.getDistanceTo(teammate) * 0.001));
        }
        else
        {
            move.setTurn(self.getAngleTo(teammate));
        }
    }
}